import { configureStore } from "@reduxjs/toolkit";
import  apiCallReducer  from "../../Redux Features/apiSlice";

export const store = configureStore({
  reducer: {
    loaderData: apiCallReducer,
  },
  middleware: (getDefaultMiddleware) => getDefaultMiddleware({
    immutableCheck:{warnAfter:128},
    serializableCheck: {warnAfter:128},
  })
});

export default store;
