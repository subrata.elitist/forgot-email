import { createSlice } from "@reduxjs/toolkit";

export const apiCallSlice = createSlice({
    name: 'loaderData',
    initialState:{
        data : null,
    },
    reducers: {
        fetchApiData: (state,action) => {
            state.data = action.payload;
        },
    },
})

export const {fetchApiData} = apiCallSlice.actions;

export default apiCallSlice.reducer;

