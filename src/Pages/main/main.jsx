import React, { StrictMode, useEffect } from "react";
import ReactDOM from "react-dom/client";
import {
  defer,
  createBrowserRouter,
  RouterProvider,
  createRoutesFromElements,
  Route,
 
} from "react-router-dom";
import App from "../App/App.jsx";
import "../../assets/styles/index.css";
import "../../assets/styles/App.css";
import ErrorPage from "../ErrorPage/ErrorPage.jsx";
import store from "../../Redux Store/store/index.jsx";
import Loader from "../../routes/loader/Loader.jsx";
import Dashboard from "../Dashboard/index.jsx";
import { Provider } from "react-redux";

const apiCall = async () => {
  // This below asynchronous function is used to send the api data to the loader Page Component with the help of defer utility and asynchronously allows other components to run side by side.
  const result = fetch("https://countriesnow.space/api/v0.1/countries").then(
    (response) => response.json()
  );

  return defer({ result }); //  in this example , we are sending this delayed values as promises instead of resolved value to the loader Page Component through (DEFER utiltiy). (More Below...)
};

const router = createBrowserRouter(
  createRoutesFromElements(
    <Route>
      <Route path="/" element={<Loader />} loader={apiCall}>
        <Route index element={<App />} />{" "}
        {/* This is where we can show to Parent Page Data onto the website */}
        <Route path="dashboard" element={<Dashboard />} />
        <Route path="*" element={<ErrorPage />} />
      </Route>
    </Route>
  )
);

ReactDOM.createRoot(document.getElementById("root")).render(
  <StrictMode>
    <Provider store={store}>
    <RouterProvider router={router} />
    </Provider>
  </StrictMode>
);

/*
 This utility allows us to defer values (to delay values) returned from loaders by passing promises instead of resolved values.
*/
