import ForgotEmail from "../ForgotEmail";
import withRedux from "../../HOC/hocWithRedux";

function App() {
  return (
    <>
      <ForgotEmail />
    </>
  );
}

export default withRedux(App);
