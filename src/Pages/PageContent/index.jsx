import React from "react";
import { Typography, Box } from "@mui/material";
import "../../assets/styles/forgotPass.css";
import MyTextField from "../../components/TextField";
import MyButton from "../../components/Buttons";

const index = () => {
  return (
    <>
      <Box className="foremail-innerPart">
        <Box className="google-words">
          <Typography variant="span" id="letter-g1">
            G
          </Typography>
          <Typography variant="span" id="letter-o1">
            o
          </Typography>
          <Typography variant="span" id="letter-o2">
            o
          </Typography>
          <Typography variant="span" id="letter-g2">
            g
          </Typography>
          <Typography variant="span" id="letter-l">
            l
          </Typography>
          <Typography variant="span" id="letter-e">
            e
          </Typography>
        </Box>
        <Box className="findYourEmai">
          <Typography className="fye" variant="paragraph">
            Find your email
          </Typography>
        </Box>
        <Box className="foremail-mode">
          <Typography className="for-mode" variant="paragraph">
            Enter your phone number or recovery email
          </Typography>
        </Box>
        <Box className="forgot-input">
          <MyTextField sx={{ width: "100%" }} label="Phone number or email" />
        </Box>
        <Box className="next-btn">
          <MyButton sx={{ width: "9ch" }} variant="contained" label="Next" />
        </Box>
      </Box>
    </>
  );
};

export default index;
