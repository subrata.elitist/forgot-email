import React, { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import PageContent from "../PageContent";
import DropDown from "../../components/dropdown";
import { Link } from "@mui/material";
import { MenuItem } from "@mui/material";
import { useSelector } from "react-redux";

const ForgotEmail = () => {
  // getting data from App props (Resolved Api data)
  const [selectedCountry, setSelectedCountry] = useState("");
  const [countries, setCountries] = useState([]);
  const getDataFromStore = useSelector((state) => state.loaderData.data);

  useEffect(() => {
    if (getDataFromStore) {
      // storing the data into setCountries as Everytime the Page Loads
      const convData = Object.values(getDataFromStore);
      setCountries(convData[2]);     // This will store only the 2nd index Datas
    } 
  }, [getDataFromStore]);

  const handleChange = (e) => {
    setSelectedCountry(e.target.value);
  };

    return (
      <>
        <Box
          className="forgotPass"
          component="span"
          sx={{
            display: "block",
            "& > :not(style)": {
              m: 1,
              height: 500,
              width: 450,
            },
          }}
        >
          <Paper
            elevation={0}
            sx={{ border: "1px solid #dadce0", margin: "0 auto" }}
          >
            <PageContent />
          </Paper>
        </Box>
        <footer>
          <Box className="footer-part">
            <Box className="language">
              <DropDown
                variant="standard"
                id="country-select-label"
                label="Country"
                labelId="country-select-label"
                value={selectedCountry}
                onChange={handleChange}
              >
                {countries.map(
                  (
                    country,
                    index // We are getting data from setCountries and map through the data to get the country result only.
                  ) => (
                    <MenuItem key={index} value={country.country}>
                      {country.country}
                    </MenuItem>
                  )
                )}
              </DropDown>
            </Box>
            <Box className="others">
              <Link href="#">Help</Link>
              <Link href="#">Privacy</Link>
              <Link href="#">Terms</Link>
            </Box>
          </Box>
        </footer>
      </>
    );
};

export default ForgotEmail;
