import {
  Await,
  Outlet,
  useLoaderData,
  useLocation,
  Navigate,
} from "react-router-dom";
import { CircularProgress, Box } from "@mui/material";
import React, { Suspense} from "react";

const Loader = () => {
  const { result } = useLoaderData(); // This is the source frm where the api data is coming from
  const location = useLocation(); // Location is used to get the current URL location from the query String
  const searchParams = new URLSearchParams(location.search); // this Object will give us the content which we search in the URL.
  const email = searchParams.get("email"); // This "GET" method will get the URL Content which is passed into the parameter.

  return (
    <>
      {email !== "123" ? <Navigate to="/" /> : ""}
      <Suspense // lets us display a fallback until its children have finished loading (Jab tak child finish nahi hoga tab tak circularProgress chalta rahega)
        fallback={
          <Box
            display="flex"
            justifyContent="center"
            alignItems="center"
            height="100vh"
          >
            {/* Circular Progress will run till the API is resolving */}
            <CircularProgress />
          </Box>
        }
      >
        <Await // Here, data comes from api which is declared inside the main.jsx page
          resolve={result}
          errorElement={<p>Some Error Occurs When Loading Data</p>}
          children={(
            resolvedResult // This will get the resolve data from above and set into children Outlet components property (CONTEXT).
          ) => <Outlet context={[resolvedResult]} />} // context data should be written inside [] braces
        />
      </Suspense>
    </>
  );
};
export default Loader;

