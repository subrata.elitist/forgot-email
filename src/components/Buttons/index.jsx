import { Button } from "@mui/material";

function myButton({label,...props}){
    return <Button {...props}>{label}</Button>
}

export default myButton;