import React from "react";
import { Box, Select,FormControl,InputLabel } from "@mui/material";

const index = ({variant,id,label, ...props }) => {
  return (
    <Box>
      <FormControl variant={variant} fullWidth>
        <InputLabel id={id}>{label}</InputLabel>
        <Select
          {...props}
        >
        </Select>
      </FormControl>
    </Box>
  );
};

export default index;
