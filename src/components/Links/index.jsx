import React from "react";
import { Link } from "@mui/material";

const index = () => {
  return (
    <div>
      <nav>
        <ul>
          <Link href="/dashboard">Dashboard</Link>
        </ul>
      </nav>
    </div>
  );
};

export default index;
