import React, { useEffect, useState } from "react";
import { fetchApiData } from "../../Redux Features/apiSlice";
import {  useDispatch } from "react-redux";
import { useOutletContext } from "react-router-dom"; // This will help to get Data which is passed within Outlet component from Loader.
const withRedux = (WrapperComponent) => {
  const WithRedux = () => {
    const [result] = useOutletContext();
    const dispatch = useDispatch();
    useEffect(() => {
      if (result) {
        dispatch(fetchApiData(result));
      }
    }, [dispatch,result]);

    return <WrapperComponent />;
  };
  return WithRedux;
};

export default withRedux;


